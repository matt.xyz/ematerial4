import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BotonesZComponent } from './botones-z.component';

describe('BotonesZComponent', () => {
  let component: BotonesZComponent;
  let fixture: ComponentFixture<BotonesZComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BotonesZComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BotonesZComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
